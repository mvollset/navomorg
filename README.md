# SQL generator for rename of groups in BMC ITSM 8.1

## Inputfiles

###environment.csv
Csv file with schemaNames and schemaIDs. Need the 72(!) schemas in the sample environment.csv

###PRE_ENVIRONMENT.sql
SQL statements with place holders for the actual tablenames. Run setenvironment.js to replace this with the schemaIDs 
from your environment. Listed in environment.csv. Pipe the output to groups.sql.
node setenvironment.js > ./inputfiles/groups.sql


### groups.sql
Add groups.sql this should include all statements needed for renaming group and support organisation. The groups.sql included is for ITSM 8.1

### map.csv
csv file containing the groups that should be renamed. Columns should be:

Support Organization;Support Group Name;SPGID;Old SO;Old GN;

##Usage:
Once the map file is present you can run the program using "node script.js". The generated scripts will can be found in outputfiles. 

