/*
BEGIN ---------
user = <user> 
company <company> 
ts = <ts> 
support_group_id = <support_group_id> 
old_support_org <old_support_org>
old_support_grp <old_support_grp>
new_support_grp <new_support_grp>
new_support_org <new_support_org>
*/
--HPD:Help Desk Assignment Log
                  UPDATE T2079 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') 
                 );
                 commit;

