/*
BEGIN ---------
user = <user> 
company <company> 
ts = <ts> 
support_group_id = <support_group_id> 
old_support_org <old_support_org>
old_support_grp <old_support_grp>
new_support_grp <new_support_grp>
new_support_org <new_support_org>
*/
--select name,schemaid from arschema where name='RKM:Configuration'
--RKM:Configuration
UPDATE T<RKM_CONFIGURATION_ID> SET C5 = '<user>', C6 = <ts>, C302300584 = '<new_support_org>',  C302300542 = '<new_support_grp>' WHERE C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>';

--RKM:KnowledgeArticleManager
UPDATE T<RKM_KNOWLEDGEARTICLE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302300584 = CASE WHEN (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302300584 END,
                 C302300542 = CASE WHEN (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C302300542 END,
                 C302300586 = CASE WHEN (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302300586 END,
                 C302300512 = CASE WHEN (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C302300512 END
                 WHERE (
                 (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') OR
                 (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>')
                 );
--RKM:ArticleHistory

  UPDATE T<RKM_ARTICLEHISTORY_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302300512 = CASE WHEN (C302300512 = '<old_support_grp>'  AND C302300544 = '<support_group_id>') THEN '<new_support_grp>' ELSE C302300512 END,
                 C302311189 = CASE WHEN (C302311189 = '<old_support_grp>'  AND C302311199 = '<support_group_id>') THEN '<new_support_grp>' ELSE C302311189 END
                 WHERE (
                 (C302300512 = '<old_support_grp>' AND C302300544 = '<support_group_id>') OR
                 (C302311189 = '<old_support_grp>' AND C302311199 = '<support_group_id>')
                 );        
--RKM:KnowledgeSources
UPDATE T<RKM_KNOWLEDGESOURCES_ID> SET C5 = '<user>', C6 = <ts>, C302300584 = '<new_support_org>',  C302300542 = '<new_support_grp>' WHERE C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>';
--FIN:ConfigCostRates
UPDATE T<FIN_CONFIGCOSTRATES_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C250100000 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C250100000 = '<old_support_grp>';
--CFG:BroadcastSPGAssociation
UPDATE T<CFG_BROADCASTSPGASSOCIATION_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CTM:CFG-ApplicationPreferences
UPDATE T<CTM_CFG-APPLICATIONPREFERENCES_ID> SET C5 = '<user>', C6 = <ts>, C301566400 = '<new_support_grp>' WHERE C301566500 IN (SELECT C1 FROM T<CTM_SUPPORT_GROUP_ID> WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

UPDATE T<CTM_CFG-APPLICATIONPREFERENCES_ID> SET C5 = '<user>', C6 = <ts>, C1000000015 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T<CTM_SUPPORT_GROUP_ID> WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

UPDATE TT<CTM_CFG-APPLICATIONPREFERENCES_ID> SET C5 = '<user>', C6 = <ts>, C1000000341 = '<new_support_grp>' WHERE C1000000427 IN (SELECT C1 FROM T<CTM_SUPPORT_GROUP_ID> WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

--CTM:SYS-Access Permission Grps
UPDATE T<CTM_SYS-ACCESS_PERMISSION_GRPS_ID> SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';

--SYS:ViewSelectionSupportGrpRoleMapping
UPDATE T<SYS_VIEWSELECTIONSUPPORTGRPROLEMAPPING_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--BMC.AM:BMC_InventoryStorage_
UPDATE T<BMC.AM_BMC_INVENTORYSTORAGE__ID>  SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:Install_ASI
UPDATE T<AST_INSTALL_ASI_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:ASTSLM:Qualbuilder
   UPDATE T<AST_ASTSLM_QUALBUILDER_ID> SET 
             C5 = '<user>', C6 = <ts>, 
             C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                                                         C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                                                         C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                                                         C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                                                         WHERE (
                                                         (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                                                         (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>')
                                                         );
--AST:Schedule Association 
UPDATE T<AST_SCHEDULE_ASSOCIATION_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:LicenseCertificates
UPDATE T<AST_LICENSECERTIFICATES_ID> SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000422 = '<new_support_grp>' WHERE C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>';

--CHG:CFG Rules
UPDATE T<CHG_CFG_RULES_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CHG:Template
UPDATE T<CHG_TEMPLATE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000001340 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001340 END,
                 C1000001339 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001339 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END
                 WHERE (
                 (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>'));


--HPD:HPDSLM:QualBuilder
       UPDATE T<HPD_HPDSLM_QUALBUILDER_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>'));   
--HPD:Template
          UPDATE T<HPD_TEMPLATE_ID> SET 
                                 C5 = '<user>', C6 = <ts>, 
                                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                                 C1000001340 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001340 END,
                                 C1000001339 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001339 END,
                                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END,
                                 C302126600 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302126600 END
                                 WHERE (
                                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                                 (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') OR
                                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') OR 
                                 (C1000000251 = '<company>'  AND C302126600 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR 
                                 (C1000000251 = '<company>'  AND C302126600 = '<old_support_org>' AND C1000000217 = '<old_support_grp>')
                                 );
--PBM:Investigation Effort Log                             
          UPDATE T<PBM_INVESTIGATION_EFFORT_LOG_ID>SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SRM:AppInstanceBridge 
          UPDATE T<SRM_APPINSTANCEBRIDGE_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--TMS:ParentApplicationObject
           UPDATE T<TMS_PARENTAPPLICATIONOBJECT_ID> SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--TMS:TaskGroupTemplate
           UPDATE T<TMS_TASKGROUPTEMPLATE_ID> SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--AAS:Activity           
           UPDATE T<AAS_ACTIVITY_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003229 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>')
                 );
--RMS:CFG Milestones
            UPDATE T<RMS_CFG_MILESTONES_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CTM:Support Group
             UPDATE T<CTM_SUPPORT_GROUP_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CTM:People Permission Groups
             UPDATE T<CTM_PEOPLE_PERMISSION_GROUPS_ID> SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';
--CTM:People_Template_SFR
             UPDATE T<CTM_PEOPLE_TEMPLATE_SFR_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--SYS:Escalation
              UPDATE T<SYS_ESCALATION_ID> SET C5 = '<user>', C6 = <ts>, C1000000015 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');
--CFG:Decision Tree
              UPDATE T<CFG_DECISION_TREE_ID> SET C5 = '<user>', C6 = <ts>, C1000001340 = '<new_support_org>',  C1000001339 = '<new_support_grp>' WHERE C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>';
--CFG:Group Event Mapping
              UPDATE T<CFG_GROUP_EVENT_MAPPING_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CFG:Scripts
              UPDATE T<CFG_SCRIPTS_ID> SET C5 = '<user>', C6 = <ts>, C1000001340 = '<new_support_org>',  C1000001339 = '<new_support_grp>' WHERE C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>';
--APR:Approver Lookup
               UPDATE T<APR_APPROVER_LOOKUP_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000001273 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000001273 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000004282 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000004282 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END,
                 C1000004284 = CASE WHEN (C1000004283 = '<company>'  AND C1000004284 = '<old_support_org>' AND C1000004285 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000004284 END,
                 C1000004285 = CASE WHEN (C1000004283 = '<company>'  AND C1000004284 = '<old_support_org>' AND C1000004285 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000004285 END;

--APR:Non-ApprovalNotifications
                 UPDATE T<APR_NON-APPROVALNOTIFICATIONS_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000001273 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CFG:Assignment
                 UPDATE T<CFG_ASSIGNMENT_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CFG:Broadcast
                 UPDATE  T<CFG_BROADCAST_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SYS:ViewSelectionPPLRoleMapping
                 UPDATE T<SYS_VIEWSELECTIONPPLROLEMAPPING_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:WorkLog
                 UPDATE T<AST_WORKLOG_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
                 

--NB Spesiell
--AST:AssetPeople               
                UPDATE T<AST_ASSETPEOPLE_ID> SET C5 = '<user>', C6 = <ts>, C260100003 = '<company>-><new_support_org>-><new_support_grp>' WHERE ((C260100003 = '<company>-><old_support_org>-><old_support_grp>')  AND (C260100013 = 'Support Group'));
--AST:CI Unavailability                
                UPDATE T<AST_CI_UNAVAILABILITY_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>')
                 );

--AST:ConfigLicenseMgmt
                 UPDATE T<AST_CONFIGLICENSEMGMT_ID> SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C301349600= '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';
--AST:ConfigurationApprovalProcess
                 UPDATE T<AST_CONFIGURATIONAPPROVALPROCESS_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:Schedule Criteria
UPDATE T<AST_SCHEDULE_CRITERIA_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--CTR:ContractBase
UPDATE T<CTR_CONTRACTBASE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C260142102 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C260142102 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );


--CHG:Infrastructure Change
                      UPDATE T<CHG_INFRASTRUCTURE_CHANGE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END,
                 C301889100 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C301889100 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );
--CHG:CHGSLM:QualBuilder
                 UPDATE T<CHG_CHGSLM_QUALBUILDER_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302113200 = CASE WHEN (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302113200 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>'));

--CHG:Infra. Change Effort Log
                 UPDATE T<CHG_INFRA._CHANGE_EFFORT_LOG_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--CHG:TemplateSPGAssoc
                 UPDATE T<CHG_TEMPLATESPGASSOC_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--RMS:TemplateSPGAssoc
                 UPDATE T<RMS_TEMPLATESPGASSOC_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--HPD:Help Desk
                 UPDATE  T<HPD_HELP_DESK_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );
--HPD:IncidentInterface_Create
                 UPDATE T<HPD_INCIDENTINTERFACE_CREATE_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--HPD:TemplateCustMapping
                 UPDATE  T<HPD_TEMPLATECUSTMAPPING_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--HPD:TemplateSPGAssoc
                 UPDATE T<HPD_TEMPLATESPGASSOC_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--PBM:Known Error
                 UPDATE T<PBM_KNOWN_ERROR_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000835 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000835 END,
                 C1000000837 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000837 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') 
                 );

--PBM:ProblemInterface_Create
                 UPDATE T<PBM_PROBLEMINTERFACE_CREATE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000001640 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001640 END,
                 C1000001639 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001639 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>')
                 );


--SRM:Request
                  UPDATE T<SRM_REQUEST_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') 
                 );
--SRM:SLMSRM:QualBuilder
                 UPDATE T<SRM_SLMSRM_QUALBUILDER_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--TMS:Task
                 UPDATE T<TMS_TASK_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C10002506 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C10002506 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') 
                 );

--TMS:TaskEffort
                 UPDATE T<TMS_TASKEFFORT_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--TMS:TaskGroup
                 UPDATE T<TMS_TASKGROUP_ID> SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--TMS:TaskTemplate
                  UPDATE T<TMS_TASKTEMPLATE_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C10002506 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>';
--RMS:Release
                  UPDATE T<RMS_RELEASE_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--RMS:Release
                  UPDATE T<RMS_RELEASE_ID> SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';

--RMS:CFG Rules
                  UPDATE T<RMS_CFG_RULES_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CTM:Support Group Alias , CTM:Support Group
                   UPDATE T<CTM_SUPPORT_GROUP_ALIAS_ID> SET C5 = '<user>', C6 = <ts>, C1000000293 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T<CTM_SUPPORT_GROUP_ID> WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>') AND C1000000073 = 0;
--SYS:ViewSelectionMasterRoleMapping
                   UPDATE T<SYS_VIEWSELECTIONMASTERROLEMAPPING_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:PurchaseRequisitionWorkLog
                   UPDATE T<AST_PURCHASEREQUISITIONWORKLOG_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CHG:ChangeInterface_Create
                    UPDATE T<CHG_CHANGEINTERFACE_CREATE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END
                 WHERE (
                 (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') 
                 );

--HPD:Help Desk Assignment Log
                  UPDATE T<HPD_HELP_DESK_ASSIGNMENT_LOG_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') 
                 );

--PBM:Problem Investigation
                 UPDATE T<PBM_PROBLEM_INVESTIGATION_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000835 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000835 END,
                 C1000000837 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000837 END,
                 C1000001640 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001640 END,
                 C1000001639 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001639 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') OR
                 (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>')
                 );
--PBM:Solution Database
                 UPDATE T<PBM_SOLUTION_DATABASE_ID> SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SRM:RequestInterface_Create
                 UPDATE T<SRM_REQUESTINTERFACE_CREATE_ID> SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') 
                 );
--TMS:Flow
                 UPDATE T<TMS_FLOW_ID>  SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
commit;
/*
END support_group_id = <support_group_id> ---------

*/