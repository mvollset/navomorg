/*
BEGIN ---------
user = <user> 
company <company> 
ts = <ts> 
support_group_id = <support_group_id> 
old_support_org <old_support_org>
old_support_grp <old_support_grp>
new_support_grp <new_support_grp>
new_support_org <new_support_org>
*/
--select name,schemaid from arschema where name='RKM:Configuration'
--RKM:Configuration
UPDATE T2766 SET C5 = '<user>', C6 = <ts>, C302300584 = '<new_support_org>',  C302300542 = '<new_support_grp>' WHERE C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>';

--RKM:KnowledgeArticleManager
UPDATE T2795 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302300584 = CASE WHEN (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302300584 END,
                 C302300542 = CASE WHEN (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C302300542 END,
                 C302300586 = CASE WHEN (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302300586 END,
                 C302300512 = CASE WHEN (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C302300512 END
                 WHERE (
                 (C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>') OR
                 (C302300585 = '<company>'  AND C302300586 = '<old_support_org>' AND C302300512 = '<old_support_grp>')
                 );
--RKM:ArticleHistory

  UPDATE T2758 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302300512 = CASE WHEN (C302300512 = '<old_support_grp>'  AND C302300544 = '<support_group_id>') THEN '<new_support_grp>' ELSE C302300512 END,
                 C302311189 = CASE WHEN (C302311189 = '<old_support_grp>'  AND C302311199 = '<support_group_id>') THEN '<new_support_grp>' ELSE C302311189 END
                 WHERE (
                 (C302300512 = '<old_support_grp>' AND C302300544 = '<support_group_id>') OR
                 (C302311189 = '<old_support_grp>' AND C302311199 = '<support_group_id>')
                 );        
--RKM:KnowledgeSources
UPDATE T2797 SET C5 = '<user>', C6 = <ts>, C302300584 = '<new_support_org>',  C302300542 = '<new_support_grp>' WHERE C302300587 = '<company>'  AND C302300584 = '<old_support_org>' AND C302300542 = '<old_support_grp>';
--FIN:ConfigCostRates
UPDATE T832 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C250100000 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C250100000 = '<old_support_grp>';
--CFG:BroadcastSPGAssociation
UPDATE T904 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CTM:CFG-ApplicationPreferences
UPDATE T969 SET C5 = '<user>', C6 = <ts>, C301566400 = '<new_support_grp>' WHERE C301566500 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

UPDATE T969 SET C5 = '<user>', C6 = <ts>, C1000000015 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

UPDATE T969 SET C5 = '<user>', C6 = <ts>, C1000000341 = '<new_support_grp>' WHERE C1000000427 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');

--CTM:SYS-Access Permission Grps
UPDATE T292 SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';

--SYS:ViewSelectionSupportGrpRoleMapping
UPDATE T1126 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--BMC.AM:BMC_InventoryStorage_
UPDATE T804 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:Install_ASI
UPDATE T1394 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:ASTSLM:Qualbuilder
   UPDATE T1653 SET 
             C5 = '<user>', C6 = <ts>, 
             C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                                                         C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                                                         C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                                                         C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                                                         WHERE (
                                                         (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                                                         (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>')
                                                         );
--AST:Schedule Association 
UPDATE T1773 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--AST:LicenseCertificates
UPDATE T1824 SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000422 = '<new_support_grp>' WHERE C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>';

--CHG:CFG Rules
UPDATE T1902 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CHG:Template
UPDATE T1949 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000001340 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001340 END,
                 C1000001339 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001339 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END
                 WHERE (
                 (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>'));


--HPD:HPDSLM:QualBuilder
       UPDATE T2078 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>'));   
--HPD:Template
          UPDATE T2106 SET 
                                 C5 = '<user>', C6 = <ts>, 
                                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                                 C1000001340 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001340 END,
                                 C1000001339 = CASE WHEN (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001339 END,
                                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END,
                                 C302126600 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302126600 END
                                 WHERE (
                                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                                 (C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>') OR
                                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') OR 
                                 (C1000000251 = '<company>'  AND C302126600 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR 
                                 (C1000000251 = '<company>'  AND C302126600 = '<old_support_org>' AND C1000000217 = '<old_support_grp>')
                                 );
--PBM:Investigation Effort Log                             
          UPDATE T2157 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SRM:AppInstanceBridge 
          UPDATE T1559 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--TMS:ParentApplicationObject
           UPDATE T1297 SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--TMS:TaskGroupTemplate
           UPDATE T1323 SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--AAS:Activity           
           UPDATE T1547 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003229 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>')
                 );
--RMS:CFG Milestones
            UPDATE T2005 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CTM:Support Group
             UPDATE T1025 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--CTM:People Permission Groups
             UPDATE T1007 SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';
--CTM:People_Template_SFR
             UPDATE T1015 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--SYS:Escalation
              UPDATE T1096 SET C5 = '<user>', C6 = <ts>, C1000000015 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>');
--CFG:Decision Tree
              UPDATE T916 SET C5 = '<user>', C6 = <ts>, C1000001340 = '<new_support_org>',  C1000001339 = '<new_support_grp>' WHERE C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>';
--CFG:Group Event Mapping
              UPDATE T927 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CFG:Scripts
              UPDATE T957 SET C5 = '<user>', C6 = <ts>, C1000001340 = '<new_support_org>',  C1000001339 = '<new_support_grp>' WHERE C1000001341 = '<company>'  AND C1000001340 = '<old_support_org>' AND C1000001339 = '<old_support_grp>';
--APR:Approver Lookup
               UPDATE T1239 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000001273 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000001273 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000004282 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000004282 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END,
                 C1000004284 = CASE WHEN (C1000004283 = '<company>'  AND C1000004284 = '<old_support_org>' AND C1000004285 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000004284 END,
                 C1000004285 = CASE WHEN (C1000004283 = '<company>'  AND C1000004284 = '<old_support_org>' AND C1000004285 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000004285 END;

--APR:Non-ApprovalNotifications
                 UPDATE T1242 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000001273 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CFG:Assignment
                 UPDATE T898 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CFG:Broadcast
                 UPDATE T902 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SYS:ViewSelectionPPLRoleMapping
                 UPDATE T1121 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:WorkLog
                 UPDATE T1407 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
                 

--NB Spesiell
--AST:AssetPeople               
                UPDATE T1383 SET C5 = '<user>', C6 = <ts>, C260100003 = '<company>-><new_support_org>-><new_support_grp>' WHERE ((C260100003 = '<company>-><old_support_org>-><old_support_grp>')  AND (C260100013 = 'Support Group'));
--AST:CI Unavailability                
                UPDATE T1816 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>')
                 );

--AST:ConfigLicenseMgmt
                 UPDATE T1699 SET C5 = '<user>', C6 = <ts>, C301363000 = '<new_support_org>',  C301363100 = '<new_support_grp>' WHERE C301349600= '<company>'  AND C301363000 = '<old_support_org>' AND C301363100 = '<old_support_grp>';
--AST:ConfigurationApprovalProcess
                 UPDATE T1711 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:Schedule Criteria
UPDATE T1774 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C260142102 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>';

--CTR:ContractBase
UPDATE T889 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C260142102 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C260142102 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C260142102 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );

--select name,schemaId from arschema where name like 'CTR%'
--CHG:Infrastructure Change
                      UPDATE T1965 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END,
                 C301889100 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C301889100 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );
--CHG:CHGSLM:QualBuilder
                 UPDATE T1904 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C302113200 = CASE WHEN (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C302113200 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000426 = '<company>'  AND C302113200 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>'));

--CHG:Infra. Change Effort Log
                 UPDATE T1938 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--CHG:TemplateSPGAssoc
                 UPDATE T1950 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--HPD:Help Desk
                 UPDATE T2116 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );
--HPD:IncidentInterface_Create
                 UPDATE T2094 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--HPD:TemplateCustMapping
                 UPDATE T2107 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--HPD:TemplateSPGAssoc
                 UPDATE T2108 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000001 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--PBM:Known Error
                 UPDATE T2190 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000835 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000835 END,
                 C1000000837 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000837 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') 
                 );

--PBM:ProblemInterface_Create
                 UPDATE T2176 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000001640 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001640 END,
                 C1000001639 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001639 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>')
                 );


--SRM:Request
                  UPDATE T1579 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') 
                 );
--SRM:SLMSRM:QualBuilder
                 UPDATE T1581 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';

--TMS:Task
                 UPDATE T1321 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C10002506 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C10002506 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') 
                 );

--TMS:TaskEffort
                 UPDATE T1309 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';

--TMS:TaskGroup
                 UPDATE T1322 SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
--TMS:TaskTemplate
                  UPDATE T1324 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C10002506 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C10002506 = '<old_support_grp>';
--RMS:Release
                  UPDATE T2049 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--RMS:CFG Rules
                  UPDATE T2008 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000015 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>';
--CTM:Support Group Alias , CTM:Support Group
                   UPDATE T1026 SET C5 = '<user>', C6 = <ts>, C1000000293 = '<new_support_grp>' WHERE C1000000079 IN (SELECT C1 FROM T1025 WHERE C1000000001 = '<company>' AND C1000000014 = '<new_support_org>' AND C1000000015 = '<new_support_grp>') AND C1000000073 = 0;
--SYS:ViewSelectionMasterRoleMapping
                   UPDATE T1120 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--AST:PurchaseRequisitionWorkLog
                   UPDATE T1766 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--CHG:ChangeInterface_Create
                    UPDATE T1905 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END
                 WHERE (
                 (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') 
                 );

--HPD:Help Desk Assignment Log
                  UPDATE T2079 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000342 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') 
                 );

--PBM:Problem Investigation
                 UPDATE T2192 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000000835 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000835 END,
                 C1000000837 = CASE WHEN (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000837 END,
                 C1000001640 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000001640 END,
                 C1000001639 = CASE WHEN (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000001639 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000000834 = '<company>'  AND C1000000835 = '<old_support_org>' AND C1000000837 = '<old_support_grp>') OR
                 (C1000000082 = '<company>'  AND C1000001640 = '<old_support_org>' AND C1000001639 = '<old_support_grp>')
                 );
--PBM:Solution Database
                 UPDATE T2193 SET C5 = '<user>', C6 = <ts>, C1000000014 = '<new_support_org>',  C1000000217 = '<new_support_grp>' WHERE C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>';
--SRM:RequestInterface_Create
                 UPDATE T1580 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') 
                 );
--TMS:Flow
                 UPDATE T1319 SET C5 = '<user>', C6 = <ts>, C1000000342 = '<new_support_org>',  C1000000341 = '<new_support_grp>' WHERE C1000000082 = '<company>'  AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>';
commit;
/*
END support_group_id = <support_group_id> ---------

*/