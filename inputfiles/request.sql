/*
BEGIN ---------
user = <user> 
company <company> 
ts = <ts> 
support_group_id = <support_group_id> 
old_support_org <old_support_org>
old_support_grp <old_support_grp>
new_support_grp <new_support_grp>
new_support_org <new_support_org>
*/
--SRM:Request
                  UPDATE T1579 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000217 = CASE WHEN (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000217 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END
                 WHERE (
                 (C1000000251 = '<company>'  AND C1000000014 = '<old_support_org>' AND C1000000217 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') 
                 );
                 
                 commit;
