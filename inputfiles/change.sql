/*
BEGIN ---------
user = <user> 
company <company> 
ts = <ts> 
support_group_id = <support_group_id> 
old_support_org <old_support_org>
old_support_grp <old_support_grp>
new_support_grp <new_support_grp>
new_support_org <new_support_org>
*/
--CHG:Infrastructure Change
                      UPDATE T1965 SET 
                 C5 = '<user>', C6 = <ts>, 
                 C1000000014 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000014 END,
                 C1000000015 = CASE WHEN (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000015 END,
                 C1000000342 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000000342 END,
                 C1000000341 = CASE WHEN (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000341 END,
                 C1000003227 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003227 END,
                 C1000003229 = CASE WHEN (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003229 END,
                 C1000003255 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003255 END,
                 C1000003256 = CASE WHEN (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003256 END,
                 C301889100 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_org>' ELSE C301889100 END,
                 C1000000422 = CASE WHEN (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000000422 END,
                 C1000003662 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_org>' ELSE C1000003662 END,
                 C1000003663 = CASE WHEN (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>') THEN '<new_support_grp>' ELSE C1000003663 END
                 WHERE (
                 (C1000000251 = '<company>' AND C1000000014 = '<old_support_org>' AND C1000000015 = '<old_support_grp>') OR
                 (C1000000082 = '<company>' AND C1000000342 = '<old_support_org>' AND C1000000341 = '<old_support_grp>') OR
                 (C1000003228 = '<company>'  AND C1000003227 = '<old_support_org>' AND C1000003229 = '<old_support_grp>') OR
                 (C1000003254 = '<company>'  AND C1000003255 = '<old_support_org>' AND C1000003256 = '<old_support_grp>') OR
                 (C1000000426 = '<company>'  AND C301889100 = '<old_support_org>' AND C1000000422 = '<old_support_grp>') OR
                 (C1000000396 = '<company>'  AND C1000003662 = '<old_support_org>' AND C1000003663 = '<old_support_grp>')
                 );
                 commit;
            
            
