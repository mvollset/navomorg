var fs = require('fs');
var csv=require('fast-csv');
fs.readFile('./inputfiles/PRE_ENVIRONMENT.sql', 'utf-8', function(err, sqldata) {
    if (err) {
        return console.log(err);
    } else {
        var stream = fs.createReadStream("./inputfiles/envrionment.csv");
        var csvStream = csv()
            .on("data", function(data) {
            	var re=new RegExp("<" + idealizeSchemaNames(data[0]) + ">","g");
            	sqldata=sqldata.replace(re,data[1]);
            })
            .on("end", function() {
                console.log(sqldata);
            });
         stream.pipe(csvStream);   
    }
});

function idealizeSchemaNames(schemaname) {
    return schemaname.replace(/\s|:/g, "_").toUpperCase() + "_ID";
}