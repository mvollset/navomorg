var async = require('async');
var filePrefix = "spg";
var csv = require('fast-csv');
var fs = require('fs');
var sqlin;
var mapgroups = [];
var outfiles = [];
var ts = Math.floor(Date.now() / 1000).toString();
var writes = [];

function writeFiles(nFiles) {
    var groupsPrFile = 0;
    if (nFiles === 0) {
        groupsPrFile = outfiles.length;
    }
    else
        groupsPrFile = Math.floor(outfiles.length / nFiles);
    var writtenFiles = 0;
    var outstring = "";
    var startGroup;
    var endGroup;
    var f;
    for (var i = 0; i < outfiles.length; i++) {
        f = outfiles[i];
        if (writtenFiles === 0)
            startGroup = f.group;
        outstring = outstring + f.sql;
        writtenFiles++;    
        if (writtenFiles == groupsPrFile) {
            endGroup = f.group;
            outstring = outstring + "\nexit;";
            writes.push({
                sql: outstring,
                startGroup: startGroup,
                endGroup: endGroup
            });
            outstring = "";
            writtenFiles = 0;
        }
        
    }
    if(outstring!=""){
        writes.push({
                sql: outstring,
                startGroup: startGroup,
                endGroup: f.group
            });
    }
}

function generateOutfiles() {
    for (var i = 0; i < mapgroups.length; i++) {
        var s = sqlin;

        var data = mapgroups[i];
        s = s.replace(/<company>/g, "NAV");
        s = s.replace(/<user>/g, "omorg");
        s = s.replace(/<ts>/g, ts);
        s = s.replace(/<old_support_org>/g, data["Old SO"]);
        s = s.replace(/<old_support_grp>/g, data["Old GN"]);
        s = s.replace(/<new_support_grp>/g, data["Support Group Name"]);
        s = s.replace(/<new_support_org>/g, data["Support Organization"]);
        s = s.replace(/<support_group_id>/g, data["SPGID"]);
        outfiles.push({
            group: data["SPGID"],
            sql: s
        });

    }
    outfiles.sort(function(a, b) {
        if (a.group < b.group)
            return -1;
        if (a.group > b.group)
            return 1;
        return 0;
    });
}
function saveFile(f,cb){
    fs.writeFile(`./outputfiles/${filePrefix}-${f.startGroup}-${f.endGroup}.sql`,f.sql,'utf-8',function(err){
        cb(err);
    });
}
async.parallel([
    function(cb) {
        var stream = fs.createReadStream('./inputfiles/map.csv');
        var csvStream = csv({
                headers: true,
                delimiter: ';'
            })
            .on("data", function(data) {
                mapgroups.push(data);
            })
            .on("end", function() {
                cb();
            });

        stream.pipe(csvStream);
    },
    function(cb) {
        fs.readFile(`./inputfiles/${filePrefix}.sql`, 'utf8', function(err, result) {
            if (err) {
                cb(err);
            }
            else {
                sqlin = result;
                cb();
            }
        });
    }
], function(err) {
    if (err) {
        console.log(err);
        process.exit(1);
    }
    generateOutfiles();
    writeFiles(2);
    async.each(writes, saveFile, function(err) {
        if (!err)
            console.log("Finished");
        else
            console.log(err);
    })
})
